<?php
// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
ini_set('memory_limit', '-1');
$db = mysqli_connect("127.0.0.1", "root", "", "test");
if ($db === false) die("Ошибка: Невозможно подключиться к MySQL " . mysqli_connect_error());
mysqli_set_charset($db, "utf8");
$result = mysqli_query($db, 'SELECT * FROM phrases');
?>
<html>
<head></head>
<body>
<?php
echo "Сейчас записей в таблице:".$result->num_rows."<br>";
$string = "{Пожалуйста,|Просто|Если сможете,} сделайте так, чтобы это {удивительное|крутое|простое|важное|бесполезное} тестовое предложение {изменялось {быстро|мгновенно|оперативно|правильно} случайным образом|менялось каждый раз}.";
// $string = "Привет {{Леша|{Алеша|Лошарик}}|Рома|{Вася|Василий|{Вассилиск|Вассилиска}}}";
echo "Обрабатываемая строка:".$string."<br>";
echo "Добавлены записи:"."<br>";
$result = recursion($string);
foreach ($result as $key => $value) {
	$response = mysqli_query($db, 'SELECT * FROM phrases WHERE content="'.$value.'" LIMIT 1');
	if($response->num_rows !== 0) continue;
	$response = mysqli_query($db, 'INSERT INTO phrases SET content = "' . $value . '"');
	echo $value."<br>";
}
// Функция ищет первый самый вложенный блок, разворачиает его, подставляет значения из блока вместо блока и передает строку в функцию
function recursion($str){
	$results = []; // Результирующий массив
	$posEnd = strpos($str,"}");
	if(!$posEnd) return [$str]; // Если блоков больше нет, товозвращаем строку обратно
	$offset = -(strlen($str)-$posEnd); // Счиатем отступ с правого края до закрывающего блока
	$posStart = strrpos($str,"{",$offset); // находим позицию где блок начинается
	$substr = explode("|",substr($str, $posStart+1,$posEnd-$posStart-1)); // Вытаскиваем значения блока и разбиваем по спец символу в массив
	foreach ($substr as $key => $value) {
		$result = recursion(substr_replace($str,$value,$posStart,$posEnd-$posStart+1));
		foreach ($result as $key => $value) {
			array_push($results,$value);
		}
	}
	return array_unique($results);
}

?>
</body>
</html>